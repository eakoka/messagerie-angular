import {Injectable, OnInit} from '@angular/core';
import {Message} from "../modeles/Message";
import {ApiMsgENIService} from "./api-msg-eni.service";
import {Observable, of} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class MsgService {
  private _tabMessage: Array<Message>;

  constructor(private api : ApiMsgENIService) {
    this._tabMessage = [];
    //let m1 : Message = new Message(new Utilisateur("jean",1), "ceci est un message");
    //this._tabMessage.push(m1);
    //let m2 : Message = new Message(new Utilisateur("marc",2), "et voici un autre message");
    //this._tabMessage.push(m2);
    this.api.appelAPI().subscribe(
      (donnees) => this._tabMessage = donnees,
      () => console.log("erreur api")

    );
  }

  get tabMessage(): Observable<Array<Message>> {
    return of(this._tabMessage);
  }

  //set tabMessage(tabMessage) {
  //  this._tabMessage = tabMessage;
  //}
  envoiMessage(message:Message) {
    this.api.retourApi(message);
  }
}

import { TestBed } from '@angular/core/testing';

import { TestHttpclientService } from './test-httpclient.service';

describe('TestHttpclientService', () => {
  let service: TestHttpclientService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TestHttpclientService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

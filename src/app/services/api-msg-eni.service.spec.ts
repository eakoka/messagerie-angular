import { TestBed } from '@angular/core/testing';

import { ApiMsgENIService } from './api-msg-eni.service';

describe('ApiMsgENIService', () => {
  let service: ApiMsgENIService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ApiMsgENIService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

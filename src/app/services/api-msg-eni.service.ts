import {Injectable, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable, of} from "rxjs";
import {Message} from "../modeles/Message";
import {Utilisateur} from "../modeles/Utilisateur";

@Injectable({
  providedIn: 'root'
})
export class ApiMsgENIService {
  tabapi: Array<any>;
  messages: Array<Message>;
  constructor(private http: HttpClient) {
    this.tabapi = [];
    this.messages = [];
    this.getAPI().subscribe(
      (donnees) => {
        this.tabapi = donnees;
        console.log(this.tabapi);
      },
      () => console.log("erreur api : recupération")
    );
    this.retourApi(new Message(new Utilisateur("wolfy", 1), "ceci est un test"));
  }

  private getAPI(): Observable<any> {
   return this.http.get("http://10.21.0.254:8080");
  }

  public appelAPI(): Observable<Array<Message>> {
    console.log(this.tabapi);
    for (let i = 0; i < this.tabapi.length; i++) {
      this.messages[i].util.pseudo = this.tabapi[i].auteur.pseudo;
      this.messages[i].util.id = 0;
      this.messages[i].dateMessage = this.tabapi[i].dateMessage;
      this.messages[i].message = this.tabapi[i].textMessage;
    }
    return of(this.messages);
  }
  public retourApi(message: Message) {
    console.log(message);
    let auteur = {
      pseudo: message.util.pseudo
    }
    let apimsg = {
      auteur: auteur,
      dateMessage: message.dateMessage,
      textMessage: message.message
    }
    console.log(apimsg);
    this.http.post<any>("http://10.21.0.254:8080", apimsg);
  }
}

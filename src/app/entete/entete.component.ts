import { Component, OnInit } from '@angular/core';
import {TestHttpclientService} from "../services/test-httpclient.service";

@Component({
  selector: 'app-entete',
  templateUrl: './entete.component.html',
  styleUrls: ['./entete.component.css']
})
export class EnteteComponent implements OnInit {
  blague: String;

  constructor(private testhttp: TestHttpclientService) {
    this.blague = '';
    this.testhttp.appelAPI().subscribe(
      (donnees) => this.blague = donnees.value,
      () => console.log("ca passe pas")
    );
  }

  ngOnInit(): void {
  }

}

import {Utilisateur} from "./Utilisateur";

export class Message {
  private _util: Utilisateur;
  private _dateMessage: Date;
  private _message: String;


  constructor(util: Utilisateur, message: String, dateMessage: Date = new Date()) {
    this._util = util;
    this._message = message;
    this._dateMessage = dateMessage;
  }


  get util(): Utilisateur {
    return this._util;
  }

  get dateMessage(): Date {
    return this._dateMessage;
  }

  set dateMessage(date: Date) {
    this.dateMessage = date;
  }

  get message(): String {
    return this._message;
  }

  set message(message: String) {
    this.message = message;
  }
}

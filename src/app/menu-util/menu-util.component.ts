import { Component, OnInit } from '@angular/core';
import {Utilisateur} from "../modeles/Utilisateur";

@Component({
  selector: 'app-menu-util',
  templateUrl: './menu-util.component.html',
  styleUrls: ['./menu-util.component.css']
})
export class MenuUtilComponent implements OnInit {
  utils: Array<Utilisateur>;
  constructor() {
    this.utils = [];
    let unutil = new Utilisateur("erwann",1);
    this.utils.push(unutil);
  }

  ngOnInit(): void {

  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuUtilComponent } from './menu-util.component';

describe('MenuUtilComponent', () => {
  let component: MenuUtilComponent;
  let fixture: ComponentFixture<MenuUtilComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MenuUtilComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuUtilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

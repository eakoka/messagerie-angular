import { Component, OnInit } from '@angular/core';
import {MsgService} from "../services/msg.service";
import {Message} from "../modeles/Message";
import {Utilisateur} from "../modeles/Utilisateur";
import {ListeMessagesComponent} from "../liste-messages/liste-messages.component";

@Component({
  selector: 'app-nouveau-message',
  templateUrl: './nouveau-message.component.html',
  styleUrls: ['./nouveau-message.component.css']
})
export class NouveauMessageComponent implements OnInit {

  constructor(private messageService: MsgService) {

  }

  ngOnInit(): void {
  }

  soumissionFormulaire(nouveauMessage: any) {
    let util: Utilisateur = new Utilisateur(nouveauMessage.pseudo, 0);
    let message: Message = new Message(util,nouveauMessage.message);
    this.messageService.envoiMessage(message);
    console.log("DEBUG : procedure, d'envoi terminé");
  }
}

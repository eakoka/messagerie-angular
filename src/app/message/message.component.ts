import {Component, Input, OnInit} from '@angular/core';
import {Message} from "../modeles/Message";

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {
  @Input()
  unMessage: Message | undefined;
  constructor() {}

  ngOnInit(): void {
  }

}

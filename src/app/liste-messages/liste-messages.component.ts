import { Component, OnInit } from '@angular/core';
import {Message} from "../modeles/Message";
import {MsgService} from "../services/msg.service";

@Component({
  selector: 'app-liste-messages',
  templateUrl: './liste-messages.component.html',
  styleUrls: ['./liste-messages.component.css']
})
export class ListeMessagesComponent implements OnInit {
  messages: Array<Message>;

  constructor(private srvMsg: MsgService) {
    this.messages = [];
  }

  ngOnInit(): void {
    this.srvMsg.tabMessage.subscribe(
      (donnees) => this.messages = donnees,
      () => console.log("tr")
    );
  }

}

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { EnteteComponent } from './entete/entete.component';
import { MenuUtilComponent } from './menu-util/menu-util.component';
import { ListeMessagesComponent } from './liste-messages/liste-messages.component';
import { MessageComponent } from './message/message.component';
import { NouveauMessageComponent } from './nouveau-message/nouveau-message.component';
import {HttpClientModule} from "@angular/common/http";
import {FormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    AppComponent,
    EnteteComponent,
    MenuUtilComponent,
    ListeMessagesComponent,
    MessageComponent,
    NouveauMessageComponent
  ],
    imports: [
        BrowserModule,
        HttpClientModule,
        FormsModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
